# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module provides a model for a monitoring station, and tools
for manipulating/modifying station data

"""


class MonitoringStation:
    """This class represents a river level monitoring station"""

    def __init__(self, station_id, measure_id, label, coord, typical_range,
                 river, town):

        self.station_id = station_id
        self.measure_id = measure_id

        # Handle case of erroneous data where data system returns
        # '[label, label]' rather than 'label'
        self.name = label
        if isinstance(label, list):
            self.name = label[0]

        self.coord = coord
        self.typical_range = typical_range
        self.river = river
        self.town = town

        self.latest_level = None

    def __repr__(self):
        d = "Station name:     {}\n".format(self.name)
        d += "   id:            {}\n".format(self.station_id)
        d += "   measure id:    {}\n".format(self.measure_id)
        d += "   coordinate:    {}\n".format(self.coord)
        d += "   town:          {}\n".format(self.town)
        d += "   river:         {}\n".format(self.river)
        d += "   typical range: {}".format(self.typical_range)
        return d

    def typical_range_consistent(self):
        """Checks the typical high/low range data for consistency.
         The method should return True if the data is consistent and False if the data is inconsistent or unavailable."""
        if self.typical_range == None:
            """Checks if there is any range data"""
            return False
        if self.typical_range[0] > self.typical_range[1]:
            """Checks if the lowest range is lower than the largest range"""
            return False
        else:
            return True
    
    def relative_water_level(self):
        """Returns the latest water level as a fraction of the typical range, 
        i.e. a ratio of 1.0 corresponds to a level at the typical high and a ratio of 0.0 corresponds to a level at the typical low."""

        if self.typical_range_consistent() == False:
            """Checks if the data is not consistent"""
            return None
        
        if self.latest_level == None:
            """Checks if latest level attribute data is not available"""
            return None
        
        frac = (self.latest_level - self.typical_range[0]) / (self.typical_range[1] - self.typical_range[0])
        """Calculates frac to be the ratio of the latest level to the typical range"""

        return frac
    


def inconsistent_typical_range_stations(stations):
    """Given a list of station objects, returns a list of stations that have inconsistent data."""
    inconsistent_stations = []
    """Initialises an empty list to be returned"""
    for i in stations:
        """Iterates through the list of stations"""
        if i.typical_range_consistent() == False:
            """Checks if the range is not consistent"""
            inconsistent_stations.append(i.name)
            """Appends the station to inconsistent_stations"""
    return inconsistent_stations