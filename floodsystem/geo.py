# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
geographical data.

"""

from floodsystem.utils import sorted_by_key  # noqa
from floodsystem.haversine import haversine # Imports haversine function from haversine dir

def stations_by_distance(stations, p):
    """ Given a list of station objects and a coordinate p, returns a list of (station, distance) tuples, where distance (float) 
    is the distance of the station (MonitoringStation) from the coordinate p. List is sorted by distance."""

    stations_coords_list = [] #Set an empty list to append tuples to

    for station in stations:
        """Iterate through list of stations. """
        temp = (station.name, haversine(station.coord, p) ) 
        """form tuple consisting of (station class, distance from p). Distance from p
        calculated using haversine function https://pypi.org/project/haversine/. """

        stations_coords_list.append(temp) # Append temp/tuple consisting of station class and distance from p to list for sorting

    return sorted_by_key(stations_coords_list, 1, False) 
"""Return list sorted from closest to furthest from point p. 1 means it sorts by the second item in the tuple. 
False is asssigned to the variable "reverse" inside the sorted_by_key function. See utils.py for function details"""



def rivers_with_station(stations):
    """Given a list of station objects, returns a set with the names of the rivers with a monitoring station."""
    
    river_liszt = [] #Initialise empty set to add rivers to.

    for i in range(len(stations)):
        """Iterate through the list of station objects."""

        river_liszt.append(stations[i].river)
        """Add each stations river to river_liszt. Note None also added?"""

    return set(river_liszt) #Returns the list as a set, removing any duplicate items.

def stations_by_river(stations):
    """Returns a dictionary that maps river names (key) to a list of station objects on a given river."""

    river_station_dict = {} #Initialise empty dictionary to add river names (keys) and stations (values)
    liszt_of_rivers = list(rivers_with_station(stations)) #Initialise set of rivers as a list to a variable (liszt_of_rivers). 

    for e in range(len(liszt_of_rivers)):
        """Iterate through list of rivers with stations"""

        stations_on_that_river = [] #Initialise empty set to add stations to, to then add as value in dictionary.
        
        for i in range(len(stations)):
            """Iterate through the list of station objects."""
            if stations[i].river == liszt_of_rivers[e]:
                """Checks to see if the river of the station at index i matches the river at index e"""

                stations_on_that_river.append(stations[i].name)
                """If the station's river matches the river at e, append that station to (stations on that river)"""

        river_station_dict[liszt_of_rivers[e]] = stations_on_that_river
        """Adds the river at index e and the list of stations on that river into a dictionary"""

    return river_station_dict #Returns the dictionary with the river (key) stations (value) pairs
        
        
def rivers_by_station_number(stations, N):
    """Determines the N rivers with the greatest number of monitoring stations. 
    Return a list of (river name, number of stations) tuples, sorted by the number of stations. 
    In the case that there are more rivers with the same number of stations as the N th entry,
    include these rivers in the list."""
    most_stations_rivers = []
    """Initialise an empty list which wil contain the rivers with the largest number of stations."""

    river_dict = stations_by_river(stations)
    """Initilise the dictionary of rivers with their stations as values"""
    counter = 0
    """Initilise counter to be used in while loop to be 0"""

    
    while counter < N:
        """While loop to iterate through the dictionary N times"""
        river_with_most_stations = []
        """Initialise river_with_most_stations as an empty list"""
        for x in river_dict:
            """Loop through the dictionary by rivers (keys)"""
            if river_with_most_stations == []:
                """For the first each river in the dictionary, do the line below."""
                river_with_most_stations = [list(river_dict.keys())[0]]
                """Initialise the list which will contain river(s) with the most number of stations in this loop.
                Initially sets the first river in the list to be the first river in river_dict."""

            elif len(river_dict[x]) > len(river_dict[river_with_most_stations[0]]):
                """Checks if the length of the value of that key is larger than the largest number of stations so far."""
                river_with_most_stations = [x]
                """Sets the list with of the rivers with the most number of stations to become that key. Overwrites any 
                previous rivers listed."""
            
            elif len(river_dict[x]) == len(river_dict[river_with_most_stations[0]]):
                """Checks if the length of the value of that key is equal to the largest number of stations so far."""

                river_with_most_stations.append(x)
                """Appends the river to the list of the rivers with the most stations."""

        for i in river_with_most_stations:
            """Loops through the list of rivers with most stations in this loop"""
            tup = (i, len(river_dict[i]))
            """Intialises the tuple which of (river, number of stations) which will be appended to returned list."""
            most_stations_rivers.append(tup)
            """Appends the rivers with the highest number of stations this loop to the rivers with most stations as a tuple
            (river, number of stations)."""
            river_dict.pop(i)
            """Removes the rivers with the most stations this loop from the dictionary of rivers"""

        counter += 1 
        """Increases the counter used in the while loop by 1"""
    return most_stations_rivers
"""Returns list of tuples"""



"""Start of task 1C"""

def stations_within_radius(stations, centre, r) :
    """This tasks takes as an input the stations, a point, and a distance, 
    and returns the stations that are within the distance given to the point"""

    close_enough_stations = [] #Empty list to store the stations that are within the distance
    stations_in_distance = stations_by_distance(stations, centre) #Get the list of stations sorted by their distance to the centre
    for i in range(len(stations_in_distance)): #Iterates through the list of stations comparing their distance to the centre to r and stores the one that are within distance in a list
        if stations_in_distance[i][1] < r:
            close_enough_stations.append(stations_in_distance[i])
    return close_enough_stations
