from .utils import sorted_by_key
from .stationdata import update_water_levels

def stations_level_over_threshold(stations, tol):
    """ returns a list of tuples, where each tuple holds (i) a station (object) at which the latest relative water level is over tol and 
    (ii) the relative water level at the station. The returned list is sorted by the relative level in descending order."""
    station_level_over_threshold_liszt = []
    """Initialises the list to be returned as an empty list"""
    
    for i in stations:
        """Iterates through the list of stations"""
        if i.relative_water_level() == None:
            """Checks if the relative_water_level data is unavaible"""
            continue
        """Skips to the next iteration of the for loop."""

        if i.relative_water_level() > tol:
            """Checks if the relative water level is over tol"""
            tup = (i, i.relative_water_level())
            """Initialises the tuple to be appended to the returned list"""
            station_level_over_threshold_liszt.append(tup)
            """Appends the tuple to the returned list"""
        
    station_level_over_threshold_liszt = sorted_by_key(station_level_over_threshold_liszt, 1, True)
    """Sorts the list by the relative level in descending order using the provided utils function"""

    return station_level_over_threshold_liszt

def stations_highest_rel_level(stations, N):
    """returns a list of the N stations (objects) at which the water level, relative to the typical range, is highest. 
    The list is sorted in descending order by relative level."""

    highest_rel_level_stations = []
    """Initialise an empty list to be returned"""
    
    stations = [stat for stat in stations if stat.relative_water_level() != None]
    """Uses list comprehension to remove the stations which have no relative water level"""
    
    for i in range(N):
        """Repeats N times"""

        if len(stations) == 0:
            """Account for the situation when the list is empty"""
            break

        highest_station_this_iteration = stations[0]
        """Initialises the highest level this iteration be the first item in the list."""

        for j in stations:
            """Iterates through list of stations"""

            if j.relative_water_level() > highest_station_this_iteration.relative_water_level():
                """Checks if the relative water level of the current station is higher than the highest level so far"""

                highest_station_this_iteration = j
                """Sets the highest level so far to be the current stations water level"""

        stations.remove(highest_station_this_iteration)
        """Removes the highest station this iteration from the stations list"""
        
        highest_rel_level_stations.append(highest_station_this_iteration)
        """Appends the highest station this iteration onto the to be returned list"""

    return highest_rel_level_stations