from floodsystem import geo, flood, stationdata
from floodsystem.datafetcher import fetch_measure_levels
from plot import plot_water_levels, plot_water_level_with_fit
import matplotlib.pyplot as plt
import matplotlib.dates
from datetime import datetime, timedelta
from analysis import polyfit

stations = stationdata.build_station_list()

stationdata.update_water_levels(stations)

flooding_stations = flood.stations_level_over_threshold(stations, 1.1)
stations_at_risk = []
towns_at_risk = []
for i in flooding_stations:
    for j in geo.stations_within_radius(stations,i[0].coord, 10):
        stations_at_risk.append(j[0])

for i in stations_at_risk:
    for j in stations:
            if i == j.name:
                    towns_at_risk.append(i)

towns_at_risk = set(towns_at_risk)
# print("Here are the towns which are considered flooded(contain stations with water level above 1.1x the highest typical range):\n")
flooding_town_names = [x[0].town for x in flooding_stations]
for i in flooding_town_names:
    if i in towns_at_risk:
        towns_at_risk.remove(i)
# print(flooding_town_names)
# print("\n")
# print("Here are the towns which are at risk (within 10 km of flooded stations):\n")
# print(towns_at_risk)

station_objects_at_risk = []
for i in towns_at_risk:
    for j in stations:
        if i == j.name:
             station_objects_at_risk.append(j)


high = []
moderate = []
low = []
high_station = []

for i in station_objects_at_risk:
    if 0.8 < i.relative_water_level() <= 0.9:
        low.append(i.town)
    if 0.9 > i.relative_water_level() <= 1.0:
        moderate.append(i.town)
    if 1.0 < i.relative_water_level() <= 1.1:
        high_station.append(i)
        high.append(i.town)

low = set(low)
moderate = set(moderate)
high = set(high)

if low == set():
    low = None
if moderate == set():
    moderate = None
if high == set():
    high = None
print("Here are the low risk towns:\n")
print(low)
print("Here are the moderate risk towns:\n")
print(moderate)
print("Here are the high risk towns:\n")
print(high)
print("Here are the severe risk towns (flooding imminent):\n")
print(flooding_town_names)


dt = 10

for i in high_station:
    dates, levels = fetch_measure_levels(i.measure_id, dt=timedelta(days=dt)) #Get the levels and dates to plot
    lowest_value = i.typical_range[0] #Obtains the lowest usual value of the water level
    highest_value = i.typical_range[1] #Obtains the highest usual value of the water level
    plt.plot(dates, [highest_value] * len(dates)) #Plots highest tipical value of water level
    plt.plot(dates, [lowest_value] * len(dates)) #Plots lowest tipical value of water level
    plot_water_levels(i, dates, levels) #Plots the level against dates
    plot_water_level_with_fit(i, dates, levels, 2)