from floodsystem import geo #Imports geo submodule
from floodsystem import stationdata #Imports stationdata submodule

station_liszt = stationdata.build_station_list() #Initialises station list to (station_liszt)
rivers_with_station_liszt = list(geo.rivers_with_station(station_liszt))
rivers_with_station_liszt.sort() #Sorts the list of rivers into alphabetical order
"""Uses rivers_with_station function from geo submodule to initialise the set of rivers with at least 
one station on them as a list under the variable (rivers_with_station_liszt)"""

print("The number of rivers which have at least one monitoring station is: %s" % len(rivers_with_station_liszt))
"""Prints all the rivers which have at least one station on them"""

print("Here are the first 10 of them in alphabetical order: " + ", ".join(rivers_with_station_liszt[:10]))

river_dict = geo.stations_by_river(station_liszt)
print("Here are the river stations on River Aire: {}".format(river_dict["River Aire"]))
print("Here are the river stations on River Cam: {}".format(river_dict["River Cam"]))
print("Here are the river stations on River Thames: {}".format(river_dict["River Thames"]))