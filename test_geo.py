from floodsystem import geo # import geo submodule
from floodsystem.stationdata import build_station_list # import stationdata submodule
import random
from floodsystem import stationdata
stations = build_station_list()

def test_stations_within_radius():
     centre = (52.2053, 0.1218)
     #Cambridge city centre coordinates
     r = 10
     #The distance within which we want the stations
     stations = stationdata.build_station_list()
     #Creates a list of stations

     close_stations = geo.stations_within_radius(stations, centre, r)

     close_stations.sort() #Order alphabetically the stations which are inside the distance

     assert type(close_stations) == type([]) #Checks that returns a list
     assert type(close_stations[0][0]) == type("a") #Checks that first element of the list is a string
     assert type(close_stations[0][1]) == type(0.0) #Checks that second element of the list is a float

def test_stations_by_distance():

     cambridge_centre = (52.2053, 0.1218)
     #Coordinates of Cambridge town centre

     stations = stationdata.build_station_list()
     #Creates a list of stations

     geo.stations_by_distance(stations, cambridge_centre)
     #Prints the list of stations sorted by distance from biggest to smallest to the centre of Cambridge

     stations_sorted_by_distance = geo.stations_by_distance(stations, cambridge_centre)
     #Empty lists for storing data
     closest_stations = []
     furthest_stations = []
     #Iterates through the list of stations and distance taking the first values and putting them into the closest stations list
     for i in range(9):
         closest_stations.append(stations_sorted_by_distance[i])

     furthest_stations = (stations_sorted_by_distance[-10:]) #Takes last 10 stations and puts them in a list

     assert furthest_stations[0][1] > closest_stations[0][1] #Checks first value of furthest is bigger than value of closest
     assert closest_stations[0][1] < furthest_stations[0][1] 
     assert type(closest_stations) == type(furthest_stations) == type([]) #Checks both are lists
     assert type(closest_stations[0][0]) == type(furthest_stations[0][0]) == type("a") #Checks first elements are strings
     assert type(closest_stations[0][1]) == type(furthest_stations[0][1]) == type(0.0) #Checks second elements are floats

def test_rivers_with_station():
    """Testing Task 1D rivers_with_station function."""
    a = geo.rivers_with_station(stations)
    """setting up a set of rivers which has a station in it"""
    assert type(a) == type(set())
    """checking that the function returns a set"""

    a = list(a)
    """seting a to be a list to iterate over its items"""

    for i in range(len(a)):
        """Iterating over a's items"""
        assert type(a[i]) == type("string")
        """Checking that each item in the list is a string"""

    assert len(a) <= len(stations)
    """Checking that the number of rivers with stations on them is less than or equal to the number of stations"""

def test_stations_by_river():
    """Testing Task 1D stations_by_river function."""
    a = geo.stations_by_river(stations)
    """Initialise a as a dictionary of rivers and their coressponding stations using geo.stations_by_river."""
    random_index = random.randint(0, len(a) - 1)
    """Initialise a random number from the range of the dictionary"""
    assert stations[random_index].name in a[stations[random_index].river]
    """Checks if the station at the random index is inside the value of the coresponding key inside the
     dictionary made by geo.stations_by_river"""

def test_rivers_by_station_number():
    """Testing Task 1E rivers_by_station_number"""
    assert type(geo.rivers_by_station_number(stations, 10)) == type([])
    """Checks if the returned data structure, is a list"""
    assert type(geo.rivers_by_station_number(stations, 10)[0]) == type(())
    """Checks if the items inside the list are tuples"""
    assert len(geo.rivers_by_station_number(stations, 18)) >= 18
    """Checks if the length of the list is more than or equal to N. 
    (As there could be rivers with the same number of stations on them)"""