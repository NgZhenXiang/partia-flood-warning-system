from floodsystem import stationdata
from floodsystem import station
a = station.inconsistent_typical_range_stations(stationdata.build_station_list())
"""Initialises (a) to be the result of calling station.inconsistent_typical_range_stations on to the list of station data"""
a.sort()
"""Sorts the list into alphabetical order"""
print(a)