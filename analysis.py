import matplotlib
import numpy as np
import matplotlib.pyplot as plt


def polyfit(dates, levels, p):
    """Given the water level time history (dates, levels) 
    for a station computes a least-squares fit of a polynomial 
    of degree p to water level data"""
    #from a list of datetime objects returns a list of float, where the floats are the time in days (including fractions of days) since the year 0001
    x = matplotlib.dates.date2num(dates) 

    # Find coefficients of best-fit polynomial f(x) of degree 4
    p_coeff = np.polyfit(x - x[-1], levels, p)

    # Convert coefficient into a polynomial that can be evaluated,
    # e.g. poly(0.3)
    poly = np.poly1d(p_coeff)   

    return poly


    