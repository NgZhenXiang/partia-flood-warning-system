import matplotlib.pyplot as plt #Import matplotlib
import datetime
import matplotlib
import numpy as np

def plot_water_levels(station, dates, levels):
    """This function plots the water level against time for a station and 
    include on the plot lines the typical values for low and high levels"""
    
    plt.plot(dates, levels) #Plot the levels against dates

    plt.xlabel('date') #Label x axis as dates
    plt.ylabel('water level (m)') #Label y axis as water level
    plt.xticks(rotation=45) #Rotate the labels on the x axis
    plt.title(station) #Sets the tittle of the station
   
    plt.tight_layout()  # This makes sure plot does not cut off date labels

    plt.show()  

def plot_water_level_with_fit(station, dates, levels, p):
    """plots the water level data and the best-fit polynomial"""
    x = matplotlib.dates.date2num(dates) 
    # Find coefficients of best-fit polynomial f(x) of degree p
    p_coeff = np.polyfit(x, levels, p)

    # Convert coefficient into a polynomial that can be evaluated,
    poly = np.poly1d(p_coeff)

    # Plot original data points
    plt.plot(x, levels, '.')

    plt.xlabel('date') #Label x axis as dates
    plt.ylabel('water level (m)') #Label y axis as water level
    plt.xticks(rotation=45) #Rotate the labels on the x axis
    plt.title(station) #Sets the tittle of the station

    # Plot polynomial fit at 30 points along interval
    x1 = np.linspace(x[0], x[-1], 30)
    plt.plot(x1, poly(x1))

    # Display plot
    plt.show()