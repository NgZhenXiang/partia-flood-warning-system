from floodsystem.stationdata import build_station_list
from floodsystem.flood import stations_highest_rel_level, update_water_levels
stations = build_station_list()
update_water_levels(stations)
highest_stations = stations_highest_rel_level(stations, 10)
"""Initialises a list of the top 10 stations with the highest relative water levels"""

print("Here are the 10 stations with the highest relative water levels.")
for i in highest_stations:
    print(i.name + " "+ str(i.relative_water_level()))