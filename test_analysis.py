import matplotlib.pyplot as plt
import numpy as np

def test_polyfit():
    """Visual confirmation by setting a parabol and then finding the closest fiting polinomial which should be the same parabol"""
    x = [-10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    y = []
    for i in range(len(x)):
        y.append(x[i] ** 2)
    print(y)
    plt.plot(x, y) #Creates a parabol


    p_coeff = np.polyfit(x, y, 2)
    poly = np.poly1d(p_coeff)
    x1 = np.linspace(x[0], x[-1], 30)
    plt.plot(x1, poly(x1)) #Creates the polynomial

    plt.show()
test_polyfit()