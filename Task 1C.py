from floodsystem import geo
#Import geo from floodsystem

from floodsystem import stationdata
#Imports the data from the stations

centre = (52.2053, 0.1218)
#Cambridge city centre coordinates
r = 10
#The distance within which we want the stations
stations = stationdata.build_station_list()
#Creates a list of stations

close_stations = geo.stations_within_radius(stations, centre, r)

close_stations.sort() #Order alphabetically the stations which are inside the distance

print (close_stations)