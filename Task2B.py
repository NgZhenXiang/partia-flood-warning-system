from floodsystem import stationdata
from floodsystem.flood import stations_level_over_threshold

stations = stationdata.build_station_list()
stationdata.update_water_levels(stations)
"""Intialises stations as the list of station objects and updates the last water level"""

print("Here are the stations which have a relative water level above 0.8 tolerance.")

for i in stations_level_over_threshold(stations, 0.8):
    """Iterates through the list of tuples consisting of station objects and relative water level"""

    print(i[0].name + " " + str(i[1]))
    """Prints the station name followed by the relative water level"""
