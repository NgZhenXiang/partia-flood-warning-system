import matplotlib.pyplot as plt #Import matplotlib
import datetime
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem import flood
from floodsystem import stationdata
from plot import plot_water_levels
from floodsystem.flood import stations_highest_rel_level, update_water_levels


stations = stationdata.build_station_list() #Creates lists of stations
update_water_levels(stations) #Updates the water level at the stations
stations_with_highest_level = stations_highest_rel_level(stations, 5) #Creates a list of 5 stations with the highest water level
station = stations_with_highest_level[0] #Takes the first station with highest level

dt = 10


print(stations_with_highest_level)
for i in range(len(stations_with_highest_level)):
    dates, levels = fetch_measure_levels(stations_with_highest_level[i].measure_id, 
                                     dt=datetime.timedelta(days=dt)) #Get the levels and dates to plot
    lowest_value = stations_with_highest_level[i].typical_range[0] #Obtains the lowest usual value of the water level
    highest_value = stations_with_highest_level[i].typical_range[1] #Obtains the highest usual value of the water level
    plt.plot(dates, [highest_value] * len(dates)) #Plots highest tipical value of water level
    plt.plot(dates, [lowest_value] * len(dates)) #Plots lowest tipical value of water level
    plot_water_levels(stations_with_highest_level[i], dates, levels) #Plots the level against dates





