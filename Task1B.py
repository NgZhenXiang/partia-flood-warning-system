from floodsystem import geo
#Import geo from floodsystem

from floodsystem import stationdata
#Imports the data from the stations

cambridge_centre = (52.2053, 0.1218)
#Coordinates of Cambridge town centre

stations = stationdata.build_station_list()
#Creates a list of stations

geo.stations_by_distance(stations, cambridge_centre)
#Prints the list of stations sorted by distance from biggest to smallest to the centre of Cambridge

stations_sorted_by_distance = geo.stations_by_distance(stations, cambridge_centre)
closest_stations = []
furthest_stations = []

for i in range(9):
  furthest_stations.append(stations_sorted_by_distance[i])
print (furthest_stations)

print (stations_sorted_by_distance[-10:])
