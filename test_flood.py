from floodsystem.flood import stations_level_over_threshold, stations_highest_rel_level
from floodsystem.station import MonitoringStation



def test_stations_level_over_threshold():
    """Test for Task 2B"""
    a = MonitoringStation("s_id_1", "m_id_1", "label_1", (3,-2), (8,10), "river_1", "town_1")
    b = MonitoringStation("s_id_2", "m_id_2", "label_2", (-12,5), (1,3), "river_2", "town_2")
    c = MonitoringStation("s_id_3", "m_id_3", "label_3", (3,4), (-8,2), "river_3", "town_3")
    d = MonitoringStation("s_id_4", "m_id_4", "label_4", (1 ,2), (-10,-5), "river_4", "town_4")
    e = MonitoringStation("s_id_5", "m_id_5", "label_5", (0, 0), (2, 1), "river_5", "town_5")
    """creating some stations for testing"""

    a.latest_level = 9
    """relative level 0.5"""
    b.latest_level = None
    """To see how function handles missing data"""
    c.latest_level = 6
    """relative level 1.4"""
    d.latest_level = -4.4
    """How function handles float values"""
    e.latest_level = 1
    """e has inconsitent typical range data"""

    liszt_of_stations = [a, b, c, d, e]
    """create list of stations to be variable in function."""

    result = stations_level_over_threshold(liszt_of_stations, 0.5)
    """set the result of calling function on made up liszt"""
    assert result[0][0].name == c.name
    assert result[0][1] == 1.4

    for i in result:
        """Iterates through list"""
        assert i[0].name != e.name
        """checks if e was added to the list"""
        assert i[0].name != b.name
        """checks if b was added to the list"""

def test_stations_highest_rel_level():
    """Test for Task 2C"""
    a = MonitoringStation("s_id_1", "m_id_1", "label_1", (3,-2), (8,10), "river_1", "town_1")
    b = MonitoringStation("s_id_2", "m_id_2", "label_2", (-12,5), (1,3), "river_2", "town_2")
    c = MonitoringStation("s_id_3", "m_id_3", "label_3", (3,4), (-8,2), "river_3", "town_3")
    d = MonitoringStation("s_id_4", "m_id_4", "label_4", (1 ,2), (-10,-5), "river_4", "town_4")
    e = MonitoringStation("s_id_5", "m_id_5", "label_5", (0, 0), (2, 1), "river_5", "town_5")
    """creating some stations for testing"""

    a.latest_level = 9
    """relative level 0.5"""
    b.latest_level = None
    """To see how function handles missing data"""
    c.latest_level = 6
    """relative level 1.4"""
    d.latest_level = -4.4
    """How function handles float values"""
    e.latest_level = 1
    """e has inconsitent typical range data"""

    liszt_of_stations = [a, b, c, d, e]
    """create list of stations to be variable in function."""

    top_5 = stations_highest_rel_level(liszt_of_stations, 3)
    """creates a list of the top 14 highest relative water level stations"""

    assert len(top_5) == 3
    """checks if the length of the top_5 is 3 as two deletions"""

    for i in range(len(top_5) - 1):
        """iterates through the list of top 14 but stops at the second last one"""
        assert top_5[i+1].relative_water_level() < top_5[i].relative_water_level()
        """checks if the next water level is less than the current one"""
