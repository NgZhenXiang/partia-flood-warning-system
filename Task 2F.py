import matplotlib.pyplot as plt #Import matplotlib
import datetime
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem import flood
from floodsystem import stationdata
from plot import plot_water_levels
from floodsystem.flood import stations_highest_rel_level, update_water_levels
from plot import plot_water_level_with_fit

stations = stationdata.build_station_list() #Creates lists of stations
update_water_levels(stations) #Updates the water levels
stations_with_highest_level = stations_highest_rel_level(stations, 5) #Creates a list of 5 stations with the highest water level
station = stations_with_highest_level[0] #Takes the first station with highest level


dt = 2
dates, levels = fetch_measure_levels(station.measure_id,
                                     dt=datetime.timedelta(days=dt)) #Get the levels and dates to plot
p = 4



for i in range(len(stations_with_highest_level)):
    dates, levels = fetch_measure_levels(stations_with_highest_level[i].measure_id, 
                                     dt=datetime.timedelta(days=dt)) #Get the levels and dates to plot
    lowest_value = stations_with_highest_level[i].typical_range[0] #Obtains lowest tipical value of water
    highest_value = stations_with_highest_level[i].typical_range[1] #Obtains highest tipical value of water
    plt.plot(dates, [highest_value] * len(dates)) #Plots highest tipical value of water level
    plt.plot(dates, [lowest_value] * len(dates)) #Plots lowest tipical value of water level
    plot_water_level_with_fit(stations_with_highest_level[i], dates, levels, p) #Plots the level against dates
