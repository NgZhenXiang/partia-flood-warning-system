import matplotlib.pyplot as plt


def test_plot_water_levels():
    """Visual confirmation that plotting is working """
    x = [1,2,3,4,5,6,7,8,9,10]
    y = x #Creates a line
    plt.plot(x,y) #Plots the line
    plt.show()
test_plot_water_levels()