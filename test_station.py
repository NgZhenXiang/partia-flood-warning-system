# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""Unit test for the station module"""

from floodsystem.station import MonitoringStation
from floodsystem.station import inconsistent_typical_range_stations
from floodsystem.stationdata import build_station_list

def test_create_monitoring_station():

    # Create a station
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    assert s.station_id == s_id
    assert s.measure_id == m_id
    assert s.name == label
    assert s.coord == coord
    assert s.typical_range == trange
    assert s.river == river
    assert s.town == town

def test_inconsistent_typical_range_stations():
    """Test for Task1F"""
    stations = build_station_list()
    """Initialises stations as a list of monitoring stations"""
    assert type(inconsistent_typical_range_stations(stations)) == type([])

def test_relative_water_level():
    """Test for Task2B"""
    a = MonitoringStation("s_id_1", "m_id_1", "label_1", (3,-2), (8,10), "river_1", "town_1")
    b = MonitoringStation("s_id_2", "m_id_2", "label_2", (-12,5), (1,3), "river_2", "town_2")
    c = MonitoringStation("s_id_3", "m_id_3", "label_3", (3,4), (-8,2), "river_3", "town_3")
    d = MonitoringStation("s_id_4", "m_id_4", "label_4", (1 ,2), (-10,-5), "river_4", "town_4")
    e = MonitoringStation("s_id_5", "m_id_5", "label_5", (0, 0), (2, 1), "river_5", "town_5")
    """creating some stations for testing"""

    a.latest_level = 9
    """relative level 0.5"""
    b.latest_level = None
    """To see how function handles missing data"""
    c.latest_level = 6
    """relative level 1.4"""
    d.latest_level = -4.4
    """How function handles float values"""
    e.latest_level = 1
    """e has inconsitent typical range data"""

    