from floodsystem import geo
#Import geo from floodsystem

from floodsystem import stationdata
#Imports the data from the stations

cambridge_centre = (52.2053, 0.1218)
#Coordinates of Cambridge town centre

stations = stationdata.build_station_list()
#Creates a list of stations

geo.stations_by_distance(stations, cambridge_centre)
#Prints the list of stations sorted by distance from biggest to smallest to the centre of Cambridge

stations_sorted_by_distance = geo.stations_by_distance(stations, cambridge_centre)
 #Creates a variable with the name of the stations sorted by distance from Cambridge centre

 #Two empty lists to store the data of closest and furthest stations
closest_stations = []
furthest_stations = []

#Takes first 9 entries of the stations (closest ones) and puts them inside their list, then prints them
for i in range(9):
  closest_stations.append(stations_sorted_by_distance[i])
print (closest_stations)

#Prints the last 10 elements of the list (furthest ones)
print (stations_sorted_by_distance[-10:])
